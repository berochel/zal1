#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;
class FunkcjaKwad
{
    int a;
    int b;
    int c;
public:
    double Delta();
    void MiejscaZerowe();
    FunkcjaKwad();
};
double FunkcjaKwad::Delta()
{
    return b*b - 4*a*c;
}
void FunkcjaKwad::MiejscaZerowe()
{
    double delta = Delta();
    if (delta<0)
        {
			if (a==0) {cout << "To nie jest funkcja kwadratowa!"; return;}
            cout << "Funkcja nie ma miejsc zerowych, bowiem delta wynosi:" << delta << endl;
        }
    else if (delta==0)
        {
            double x = -b/(2*a);
            cout << "Funkcja ma jedno miejsce zerowe: " << fixed << setprecision(4) << x << ", a delta wynosi:" << delta << endl;
        }
    else
        {
            double xa = (-b - sqrt(delta))/(2*a);
            double xb = (-b + sqrt(delta))/(2*a);
            cout << "Funkcja ma dwa miejsca zerowe: " << fixed << setprecision(4) << xa << " oraz " << xb << ", a delta wynosi:" << delta << endl;
        }
}
FunkcjaKwad::FunkcjaKwad()
{
    cout << "Podaj wartosc wspolczynnika przy x^2:";
    cin >> a;
    cout << "Podaj wartosc wspolczynnika przy x:";
    cin >> b;
    cout << "Podaj wartosc wspolczynnika wyrazu wolnego:";
    cin >> c;
}
int main()
{
    cout << "Witaj w programie wyznaczajacym miejsca zerowe funkcji kwadratowej." << endl;
    FunkcjaKwad* funkcja = new FunkcjaKwad;
    funkcja->MiejscaZerowe();
    delete funkcja;
    // SKOMPILOWANE W C.OUT cout << "Delta, jakby ktos nie zauwazyl, wynosi: " << fixed << setprecision(4) << funkcja->Delta() << endl;

    cout << "Pomyslnie usunieto obiekt klasy!";
}
